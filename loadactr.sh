#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)
LOAD_FILE_NAME="load-act-r.lisp"

echo "${SCRIPT_DIR}/${LOAD_FILE_NAME}"

sbcl --dynamic-space-size 16gb --load "${SCRIPT_DIR}/${LOAD_FILE_NAME}"