import numpy as np
import matplotlib.pyplot as plt
import rl.MazeBase as mb
import math

class AgentBase():
    def _init_cout_map(self, width, height):
        return [[0 for _ in range(width)] for _ in range(height)]
    
    def _calc_entropy(self, count_map, conner_points, k=1):
        total_count = np.sum(count_map)
        entropy = 0
        for conner_point in conner_points:
            p = count_map[conner_point.y, conner_point.x] / total_count
            #確率0の時は情報量0とする
            if p > 0:
                entropy += - p * math.log(p)

        return entropy * k
    
    def _calc_probabilities(self, count_map, conner_points):
        total_count = np.sum(count_map)
        probabilities = []
        for conner_point in conner_points:
            p = count_map[conner_point.y, conner_point.x] / total_count
            probabilities.append(p)

        return probabilities

    def _save_heatmaps(self, file_name, maze_env, count_maps, labels, base_index, step, col_num):
        a = np.array(maze_env.maze_map)
        na = np.where( a == mb.MazeBase.GOAL, mb.MazeBase.PATH, a)
        height = na.shape[0]
        width = na.shape[1]
        cmap = plt.get_cmap("Reds")
        
        target_map_num = math.ceil((len(count_maps) - (base_index+1)) / step)
        row_num = target_map_num // col_num
        if row_num % col_num > 0:
            row_num += 1

        scale = 6   #値は適当
        fig = plt.figure(figsize=(col_num * scale, row_num *scale))
        cnt = 0
        for i in range(base_index, len(labels), step):
            count_map_n = count_maps[i] / np.linalg.norm(count_maps[i])
            ax = fig.add_subplot(row_num, col_num, cnt+1)
            ax.set_xticks([])   #x軸表示なし
            ax.set_yticks([])   #y軸表示なし
            ax.set_xlabel(labels[i], fontsize=24)
            image = []
            for y in range(height):
                image_line = []
                for x in range(width):
                    if na[y, x] == 1:
                        # 壁の部分を透明にする
                        pixel = (0, 0, 0, 0)
                    else:
                        pixel = cmap(count_map_n[y, x])
                    image_line.append(pixel)
                image.append(image_line)
            ax.imshow(na, cmap="binary")
            ax.imshow(image)
            ax.plot(maze_env.start_point.x, maze_env.start_point.y, "D", color='y', markersize=2 * scale)
            ax.plot(maze_env.goal_point.x, maze_env.goal_point.y, "D", color='g', markersize=2 * scale)
            for conner_point in maze_env.conner_points:
                ax.text(conner_point.x, conner_point.y, round(count_map_n[conner_point.y, conner_point.x], 3), fontsize=10, horizontalalignment="center", verticalalignment="center")            
            cnt += 1

        plt.savefig(file_name)
        plt.cla()

    def _save_heatmap(self, file_name, maze_env, count_map):
        a = np.array(maze_env.maze_map)
        na = np.where( a == mb.MazeBase.GOAL, mb.MazeBase.PATH, a)
        height = na.shape[0]
        width = na.shape[1]
        cmap = plt.get_cmap("Blues")
        image = []
        for y in range(height):
            image_line = []
            for x in range(width):
                if na[y, x] == 1:
                    # 壁の部分を透明にする
                    pixel = (0, 0, 0, 0)
                else:
                    pixel = cmap(count_map[y, x])
                image_line.append(pixel)
            image.append(image_line)
        plt.imshow(na, cmap="binary")
        plt.imshow(image)
        plt.plot(maze_env.start_point.x, maze_env.start_point.y, "D", color="tab:red", markersize=10)
        plt.plot(maze_env.goal_point.x, maze_env.goal_point.y, "D", color="tab:green", markersize=10)

        for conner_point in maze_env.conner_points:
            plt.text(conner_point.x, conner_point.y, str(conner_point.no), horizontalalignment="center", verticalalignment="center")
        plt.savefig(file_name)
        plt.cla()

if __name__ == '__main__':
    pass