from enum import Enum
import numpy as np
import rl.MazeBase as mb

class Action(Enum):
    West = 1
    North  = 2
    East = 4
    South = 8

class BrainEnvironment():
    _DEFAULT_REWARD = 0
    _UNCHANGED_REWARD = -1
    _GOAL_REWARD = 10

    def __init__(self):
        self._maze_env = None

    @property
    def row_length(self):
        return len(self._maze_env.maze)

    @property
    def column_length(self):
        return len(self._maze_env.maze[0])

    @property
    def actions(self):
        return [Action.West, Action.North,
                Action.East, Action.South]

    @property
    def action_space_n(self):
        return len(Action)

    @property
    def maze_env(self):
        return self._maze_env

    def _move(self, state, action):
        next_state = state.clone()

        state_info = self._maze_env.state_inf_dic[state]
        next_no = state_info.connected_no[action.value]
        if next_no is not None:
            next_state = self.noToState(next_no)

        return next_state

    def reward_func(self, state, next_state):
        done = False
        reward = BrainEnvironment._DEFAULT_REWARD
        attribute = self._maze_env.maze_map[state.row][state.column]
        if attribute == mb.MazeBase.PATH or attribute == mb.MazeBase.START:
            if next_state.row == state.row and next_state.column == state.column:
                reward = BrainEnvironment._UNCHANGED_REWARD
        elif attribute == mb.MazeBase.GOAL:
            reward = BrainEnvironment._GOAL_REWARD
            done = True

        return reward, done

    def load_maze_env(self, maze_env):
        self._maze_env = maze_env

    def reset(self):
        self.agent_state = mb.State(self._maze_env.start_point.y, self._maze_env.start_point.x)
        return self.agent_state

    def noToState(self, no):
        states = [k for k, v in self._maze_env.state_inf_dic.items() if v.no == no]
        if len(states) == 0:
            return None
        else:
            #2つ以上の要素を持つことはないはず
            return states[0]

    def step(self, action):
        next_state, reward, done = self.transit(self.agent_state, action)
        if next_state is not None:
            self.agent_state = next_state

        return next_state, reward, done

    def transit(self, state, action):
        next_state = self._move(state, action)
        reward, done = self.reward_func(state, next_state)
        return next_state, reward, done
