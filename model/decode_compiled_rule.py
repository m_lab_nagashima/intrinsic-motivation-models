import re
import networkx as nx
import matplotlib.pyplot as plt

class Rule:
    __move_rule_names = ['MOVE-CASED-BASE']
    # __move_rule_names = ['MOVE-CASED-BASE', 'BACKTRACK-END', 'PREDICT6']  #instance-base
      # __move_rule_names = ['BACKTRACK-END', 'PREDICT6']  #backtrack
      # __move_rule_names = ['PREDICT3']  #random
    __move_rule_table = {}  #判定のメモ化

    def __init__(self, name, production_text, parents, chunk):
        self.__name = name
        self.__production_text = production_text
        self.__parents = parents
        self.__chunk = chunk
        self.__children = []

    def __str__(self):
        name = self.__name
        parents = 'None'
        if len(self.parents) > 0:
            parents = '%s,%s' % (self.parents[0].name, self.parents[1].name)
        children = 'None'
        if len(self.children) > 0:
            children = '%s' % self.children[0].name
            for c in self.children[1:]:
                children += ',%s' %(c.name)
        production = self.production_text
        return 'name=%s, parents=%s, children=%s, production=\n%s' % (name, parents, children, production)

    def __is_move_rule(self, rule):
        l = list(filter(lambda f: f == rule.name, Rule.__move_rule_names))
        if len(l) == 1: return True

        cnt = 0
        for p in rule.parents:
            cnt += 1 if self.__is_move_rule(p) == True else 0
        
        return cnt > 0

    @property
    def name(self):
        return self.__name

    @property
    def production_text(self):
        return self.__production_text

    @property
    def parents(self):
        return self.__parents

    @property
    def children(self):
        return self.__children
    
    @property
    def chunk(self):
        return self.__chunk
    
    @property
    def move_rule_table(self):
        return self.__move_rule_table

    @property
    def is_move_rule(self):
        if self.name in Rule.__move_rule_table : return Rule.__move_rule_table[self.name]
        b = self.__is_move_rule(self)
        Rule.__move_rule_table[self.name] = b
        return b

    @property
    def is_move_root_rule(self):
        return len(list(filter(lambda name: name == self.name, Rule.__move_rule_names))) == 1

class Chunk:
    def __init__(self, name, chunk_text, content):
        self.__name = name
        self.__chunk_text = chunk_text
        self.__content = content

    def __str__(self):
        name = self.__name
        chunk = self.__chunk_text
        return 'name=%s, chunk=\n%s' % (name, chunk) 

    @property
    def name(self):
        return self.__name

    @property
    def chunk_text(self):
        return self.__chunk_text

    @property
    def content(self):
        return self.__content

class ChunkContentBase:
    pass

class InfoChunkContent(ChunkContentBase):
    def __init__(self, from_slot, to_slot):
        self.__from_slot = from_slot
        self.__to_slot = to_slot

    @staticmethod
    def bulid_chunk_text(chunk_text):
        m = re.search(r'(FROM|STACK-DATA-FROM|ROAD-NO)\s+\d+', chunk_text)
        if m is None : raise ValueError
        m = re.search(r'\d+', m.group())
        from_slot = int(m.group())

        m = re.search(r'(TO|STACK-DATA-FROM|ROAD-FOWARD-NO)\s+\d+', chunk_text)
        if m is None : raise ValueError
        m = re.search(r'\d+', m.group())
        to_slot = int(m.group())

        return InfoChunkContent(from_slot, to_slot)

    @property
    def from_slot(self):
        return self.__from_slot

    @property
    def to_slot(self):
        return self.__to_slot

class ActrParserManger:
    def __init__(self, production_path, chunk_path):
        self.__production_path = production_path
        self.__chunk_path = chunk_path
        self.__rules = []
        self.__rule_table = {}
        self.__chunks = []
        self.__parse_chunks_from_file()
        self.__parse_rules_from_file()
        self.__register_rule()
        self.__register_children()

    def __str__(self):
        s = ''
        for rule in self.__rules:
            s += str(rule)
        return s

    @property
    def path(self):
        return self.__path

    @property
    def rules(self):
        return self.__rules

    @property
    def root_rules(self):
        return list(filter(lambda r: len(r.parents) == 0, self.__rules))

    def __parse_chunks_from_file(self):
        stack = []
        strings = ''
        with open(self.__chunk_path) as f:
            strings = f.read()
        
        i = 0
        while i < len(strings):
            if len(stack) == 0 and strings[i] == '(':
                j = i
                while True:
                    if strings[j] == '(':
                        stack.append(strings[j])
                    elif strings[j] == ')':
                        stack.pop()
                    if len(stack) == 0:
                        chunk_text = strings[slice(i,j+1, 1)]
                        name = self.__parse_chunk_name(chunk_text)
                        content = None
                        if 'INFO' in name or ('CHUNK' in name and not 'DATA' in chunk_text):
                            content = InfoChunkContent.bulid_chunk_text(chunk_text)
                        chunk = Chunk(name, chunk_text, content)
                        self.__chunks.append(chunk)
                        i = j
                        break
                    j += 1
            i += 1

    def __parse_rules_from_file(self):
        stack = []
        strings = ''
        with open(self.__production_path) as f:
            strings = f.read()
        
        i = 0
        while i < len(strings):
            if len(stack) == 0 and strings[i] == '(':
                j = i
                while True:
                    if strings[j] == '(':
                        stack.append(strings[j])
                    elif strings[j] == ')':
                        stack.pop()
                    if len(stack) == 0:
                        rule_text = strings[slice(i,j+1, 1)]
                        name = self.__parse_rule_name(rule_text)
                        rule = Rule(name, rule_text, self.__get_parents(rule_text), self.__get_chunk(rule_text))
                        self.__rules.append(rule)
                        i = j
                        break
                    j += 1
            i += 1

    def __register_rule(self):
        for rule in self.__rules:
            self.__rule_table[rule.name] = rule

    def __register_children(self):
        for rule in self.__rules:
            if len(rule.parents) == 0: continue
            for p in rule.parents:
                if not p.name in self.__rule_table: raise ValueError    #親の名前があるのに、テーブルに親が登録されていないということはない
                self.__rule_table[p.name].children.append(rule)

    def __get_chunk(self, rule_text):
        chunk_name = self.__parse_involved_chunk_name(rule_text)
        l = list(filter(lambda c: c.name == chunk_name, self.__chunks))
        return l[0] if len (l) == 1 else None

    def __get_parents(self, rule_text):
        parent_names = self.__parse_parents(rule_text)
        if len(parent_names) != 2: return []
        parents = []
        for p_name in parent_names:
            l = list(filter(lambda r: r.name == p_name, self.__rules))
            if len(l) != 1 : raise ValueError    #このときに親が1以外見つかるということはおかしい
            parents.append(l[0])
        return parents

    def __parse_parents(self, rule):
        m = re.search(r'[\w-]+ & [\w-]+', rule)
        if not m is None:
            parents = m.group().split('&')
            return list(map(lambda s: s.strip(), parents)) 
        return []        
    
    def __parse_chunk_name(self, chunk_text):
        m = re.match(r'\([\w-]+[\r\n]+', chunk_text)
        return m.group()[slice(m.start() + 1, m.end()-1)]
    
    def __parse_rule_name(self, rule):
        m = re.match(r'\(P [\w-]+[\r\n]+', rule)
        return m.group()[slice(m.start() + 3, m.end()-1)]

    def __parse_involved_chunk_name(self, rule):
        m = re.search(r'[\w-]+ - [\w-]+', rule)
        if not m is None:
            l = list(map(lambda t: t.strip(), m.group().split(' - ')))
            return l[1] if len(l) == 2 else ''
        return ''

def show_move_rule_tree(rules):
    G = nx.DiGraph()
    plt.figure(figsize=(10, 10))

    labels = {}
    colors = []
    for r in rules:
        if not r.is_move_rule : continue
        G.add_node(r.name)
        labels[r.name] = re.sub('PRODUCTION' ,'', r.name)
        if r.is_move_root_rule:
            colors.append('g')
        else:                        
            colors.append('y')
        if not r.chunk is None:
            if not r.chunk.name in G.nodes:
                description = ''
                if not r.chunk.content is None:
                    description = '\nfrom:' + str(r.chunk.content.from_slot) + '\nto:' + str(r.chunk.content.to_slot)
                G.add_node(r.chunk.name)
                labels[r.chunk.name] = re.sub('^\w+',r.chunk.name[0],re.sub('-\d+$' ,'',r.chunk.name)) + description
                colors.append('r')

            G.add_edge(r.name, r.chunk.name)

        for p in r.parents:
            if p.is_move_rule : G.add_edge(p.name, r.name)
    pos = nx.spring_layout(G, k=2)
    nx.draw(G, pos=pos, node_color=colors, labels = labels, node_size=2000)
    plt.show()

if __name__ == '__main__':
    actr = ActrParserManger('productions.log', 'chunks.log')
    show_move_rule_tree(actr.rules)