from collections import defaultdict
from agent_base import AgentBase
from brain_environment import Action, BrainEnvironment
import copy
import math
import pandas as pd
import os
import sys
import rl.MazeWallExtended as mwe
import csv
import numpy as np

class MotivatedBrainOkaAgent(AgentBase):

    def __init__(self, epsilon=0.2):
        self.epsilon = epsilon
        self.base_tau = 0
        self.DF_TRIAL_COLUMNS = ['map_no', 'reward','tau' ,'round', 'steps', 'goal_rate', 'entropy', 'entropy_n', 'probability']
        super().__init__()

    def policy(self, s, actions):
        if np.random.random() < self.epsilon:
            return np.random.choice(actions)
        else:
            if s in self.Q and sum(self.Q[s]) != 0:
                return actions[np.argmax(self.Q[s])]
            else:
                return np.random.choice(actions)

    def _init_episode(self):
        self.Q = {}

    def noize(self, s, mu=0):
    # 逆変換法で一様分布からロジスティック分布を得る
        np.random.seed()
        N = 1
        U = np.random.uniform(0.0, 1.0, N)
    # ロジスティック分布の累積分布関数の逆関数を用いて変換
        V = mu + s * np.log(U / (1 - U))
        return V[0]    
    
    def calc_internal_reward(self, tau, old_state, current_state, action):
        if old_state == current_state:
            return 0
        if sum(self.Q[old_state]) == 0:
            return 0

        min_value = min(self.Q[old_state])
        work_Q = list(map(lambda x: x - min_value, self.Q[old_state]))

        sumq = sum(work_Q)
        if sumq == 0:
            return 0

        q =  1 - work_Q[action.value] / sumq
        if q == 0:
            return 0

        return - tau * q * math.log(q)

    def _init_learn(self):
        self.map_nos = []
        self.rewards = []
        self.taus = []
        self.rounds = []
        self.steps = []
        self.goal_rates = []
        self.entropies = []
        self._entropies_n = []
        self._probabilities = []

    def _save_recoding_trial_log(self, log_name):
        df = pd.DataFrame(
            data={
                'map_no': self.map_nos,
                'reward': self.rewards,
                'tau': self.taus,
                'round': self.rounds,
                'steps' : self.steps,
                'goal_rate': self.goal_rates,
                'entropy': self.entropies,
                'entropy_n': self._entropies_n,
                'probability' : self._probabilities
            },
            columns = self.DF_TRIAL_COLUMNS
        )

        if os.path.exists(log_name) == True:
            df.to_csv(log_name, index=False, header=False, mode='a')
        else:
            df.to_csv(log_name, index=False, header=True, mode='w')

    def _record_trial(self, map_no, reward, tau, round, step, goal_rate, entropy, entropy_n, probability):
        self.map_nos.append(map_no)
        self.rewards.append(reward)
        self.taus.append(tau)
        self.rounds.append(round)
        self.steps.append(step)
        self.goal_rates.append(goal_rate)
        self.entropies.append(entropy)
        self._entropies_n.append(entropy_n)
        self._probabilities.append(probability)

    def do_episode(self, env, map_no, count_map, current_trial_num, gamma, learning_rate, stop_threshold, reward, limit_step, division_rate):
        actions = list(env.actions)
        self.Q = {}
        self.Q = defaultdict(lambda: [0] * len(actions))
        i_reward_sum = 0.0

        round_num = 0
        tau = self.base_tau + (reward-1)/division_rate
        steps = 0
        clear = 0.0
        while True:
            egs = self.noize(1)
            eval_value = i_reward_sum + self.noize(egs)
            if round_num > 1 and eval_value <= stop_threshold:
                break
            s = env.reset()
            n_state = s
            done = False
            step = 0
            i_reward_sum = 0.0
            while not done and step < limit_step:
                a = self.policy(s, actions)
                n_state, e_reward, done = env.step(a)
                if n_state != s:
                    count_map[n_state.row, n_state.column] += 1
     
                max_QValue = max(self.Q[n_state])
                i_reward = self.calc_internal_reward(tau, n_state, s, a)
                i_reward_sum += i_reward
                gain = e_reward + i_reward + gamma * max_QValue
                estimated = self.Q[s][a.value]
                self.Q[s][a.value] += learning_rate * (gain - estimated)
                s = n_state
                step += 1
            round_num += 1
            steps += step
            clear += float(done)
        self._record_trial(map_no, reward, tau, round_num, steps, clear/round_num, self._calc_entropy(count_map, env.maze_env.conner_points), self._calc_entropy(count_map, env.maze_env.conner_points, 1.0/math.log( len(env.maze_env.conner_points))), ":".join(map(str,self._calc_probabilities(count_map, env.maze_env.conner_points))))

    def learn(self, env, map_no, traial_num, gamma, learning_rate, stop_threshold, reward_min, reward_max, limit_step, base_tau, division_rate):
        self.base_tau = base_tau
        width = env.maze_env.width
        height = env.maze_env.height
        log_name = "rl_result_{}_{}_{}_{}_{}_{}_{}_{}.csv".format(width,height, traial_num,reward_min, reward_max, limit_step, base_tau, division_rate)
        rewards = [i for i in range(reward_min, reward_max+1)]
        count_maps = []
        for reward in rewards:
            self._init_learn()
            count_map_sum = np.zeros((height, width))
            for i in range(traial_num):
                count_map = np.zeros((height, width))
                self._init_episode()
                self.do_episode(env, map_no, count_map, i+1, gamma, learning_rate , stop_threshold, reward, limit_step, division_rate)
                count_map_sum += count_map
            print("end map_no={}, reward={}".format(map_no, reward))
            self._save_recoding_trial_log(log_name)
            #self._save_heatmap("rl_heat_map_{}_{}_{}_{}.png".format(width, height, map_no, reward), env.maze_env, count_map_sum/traial_num)
            count_maps.append(count_map_sum/traial_num)
        labels = [ 'tau={}'.format(round(self.base_tau + (r-1)/division_rate, 2)) for r in range(reward_min, reward_max + 1)]
        base_index = 4
        base_step = 5
        col_num = 2
        self._save_heatmaps("rl_heat_map_{}_{}_{}.png".format(width, height, map_no), env.maze_env, count_maps, labels, base_index, base_step, col_num)

maze_difficulties = [(7,7), (9,9), (11,11)]

def generate_env(num):
    maze_env = mwe.MazeWallExtended()
    for d in maze_difficulties:
        width = d[0]
        height = d[1]
        dir_name = str(width) + "_" + str(height)
        if os.path.isdir(dir_name):
            continue

        os.makedirs(dir_name, exist_ok=True)
        for i in range(0, num):
            maze_env.create_maze(width, height)
            with open("{}\{:03d}.csv".format(dir_name, i),mode='w', newline="") as f:
                writer = csv.writer(f)
                for row in maze_env.maze_map:
                    writer.writerow(row)    

def train():
    traial_num = 10000
    gamma = 0.9
    learning_rate = 0.2
    stop_threshold = 5
    reward_min = 1
    reward_max = 20
    limit_step = 100
    base_tau = 0.35
    division_rate = 50

    args = sys.argv
    if len(args) > 1:
        traial_num = int(args[1])
        reward_min = int(args[2])
        reward_max = int(args[3])
        limit_step = int(args[4])
        base_tau = float(args[5])
        division_rate = float(args[6])

    agent = MotivatedBrainOkaAgent()

    maze_table = []
    for d in maze_difficulties:
        width = d[0]
        height = d[1]        
        dir_name = str(width) + "_" + str(height)
        maze_env = mwe.MazeWallExtended()
        cnt = 0
        mazes = []
        while True:
            path = "{}\\{}\\{:03d}.csv".format(os.getcwd(), dir_name, cnt)
            if not os.path.exists(path):
                break
            
            m = []
            with open(path, "r") as f:
                reader = csv.reader(f)
                for line in reader:
                    m.append([int(n) for n in line])
            
            mazes.append(m)
            cnt+=1
        maze_table.append(mazes)
    
    for ms in maze_table:
        cnt = 0
        for m in ms:
            maze_env.load_maze(m)
            maze_env.print_maze()
            env = BrainEnvironment()
            env.load_maze_env(maze_env)
            height = len(m)
            width = len(m[0])
            agent.learn(env, cnt, traial_num, gamma, learning_rate, stop_threshold, reward_min, reward_max, limit_step, base_tau, division_rate)
            cnt += 1

if __name__ == "__main__":
    generate_env(10)
    train()
