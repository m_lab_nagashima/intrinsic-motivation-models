(defun pprint-save-chunk (chunk-name)
  (let ((chunk (get-chunk chunk-name)))
    (when chunk
      (command-output
       (format nil "  (~S~%~@[~S~%~]    ISA CHUNK~%~{~{     ~s  ~s~}~^~%~})"
         chunk-name
         (act-r-chunk-documentation chunk)
         (mapcar (lambda (slot) 
                   (list (act-r-slot-name (car slot)) (cdr slot)))
           (sort (copy-tree (act-r-chunk-slot-value-lists chunk)) #'< :key (lambda (x) (act-r-slot-index (car x))))))))))

(defvar *default-chunks*
    (let ((name (do ((n (new-symbol "dummy") (new-symbol "dummy")))
                    ((not (find n (mp-models))) n))))
      (prog2
        (define-model-fct name nil)
          (with-model-eval name
            (chunks))
        (delete-model-fct name))))

(defun initialize-utility-for-compiled-production (new-p p1 p2)
  (trigger-reward *reward-value*)
  (let* ((at1 (production-at p1))
         (at2 (production-at p2))
         (at (max at1 at2))
         (r1 (production-reward p1))
         (r2 (production-reward p2))
         (reward (or (and (numberp r1)
                          (numberp r2)
                          (max r1 r2))
                     (and (numberp r1) r1)
                     (and (numberp r2) r2)
                     r1 r2)))
    (setf (production-at new-p) at)
    ;;(setf (production-reward new-p) reward)
    (setf (production-u new-p) (car (no-output (sgp :nu))))))

(defun save-productions (file-name &optional (zero-ref t))
  (if (probe-file file-name)
    (delete-file file-name))

  (let* ((productions (no-output (pp)))
         (cmdt (car (no-output (sgp :cmdt)))))
    
    (unwind-protect 
        (progn
          ;;; Use the command trace to write things out since it will 
          ;;; handle the opening of the file and pprint-chunks and pp will then work
          ;;; "automatically".
          (sgp-fct (list :cmdt file-name))

          ;; write out productions          
          (pp)
      
      ;;; set the command trace back to where it was
      (sgp-fct (list :cmdt cmdt))))))

(defun save-chanks (file-name &optional (zero-ref t))
  (if (probe-file file-name)
    (delete-file file-name))

  (let* ((dm-chunks (no-output (dm)))
          (other-chunks (set-difference (set-difference (chunks) *default-chunks*) dm-chunks))
         (cmdt (car (no-output (sgp :cmdt)))))
    
    (unwind-protect 
        (progn
          ;;; Use the command trace to write things out since it will 
          ;;; handle the opening of the file and pprint-chunks and pp will then work
          ;;; "automatically".
          (sgp-fct (list :cmdt file-name))
          (dolist (x (reverse (append other-chunks dm-chunks)))
            (pprint-save-chunk x))

      ;;; set the command trace back to where it was
      (sgp-fct (list :cmdt cmdt))))))

(defvar *reward-value* 0)
(defvar *visited-points* nil)

(defun visit-point (point)
  (setf *visited-points* (cons (list point (mp-time)) *visited-points*)))

(defun get-visited-points ()
  (reverse *visited-points*))

(defun clear-points ()
  (setf *visited-points* nil))

(defun get-west (checked-dir)
  (logand checked-dir #b0001))

(defun get-north (checked-dir)
  (logand checked-dir #b0010))

(defun get-east (checked-dir)
  (logand checked-dir #b0100))

(defun get-south (checked-dir)
  (logand checked-dir #b1000))

(defun check-dir (checkd-dir dir)
  (logior checkd-dir dir))

(defun get-back-dir(dir)
  (let* ((shifted-value (ash dir 2)))
    (if (> (logand shifted-value #b00110000) 0)
      (ash shifted-value -4)
      shifted-value)))

(defun set-reward-value (value)
  (setf *reward-value* value))